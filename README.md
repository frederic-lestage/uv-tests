### Usage

``` bash
$ mkdir build
$ mkdir dist
$ cd build
$ cmake ../core && cmake --build .
$ ./fred  # help
$ ./fred ../src/test.obj test 1  # input output method
```

### Hierarchy
 - **core/**: main scripts
   - **math/**: simplified math library that can be replaced by an actual math library
   - **uv_methods/**: concrete implementations of uv generation
 - **dist/**: output .obj files
 - **src/**: .obj test files (replace mesh data from calimiro or Splash)
