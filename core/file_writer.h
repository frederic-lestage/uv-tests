#ifndef FILE_WRITER_H
#define FILE_WRITER_H

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

// Specific file wrtier for an .obj
// https://en.wikipedia.org/wiki/Wavefront_.obj_file
class ObjFileWriter {
    private:
        std::string _filename; // name of the output file
        std::vector<std::string> _lines; // lines of the output file

        const std::string vertices_comment = "List of geometric vertices, with (x, y, z [,w]) coordinates, w is optional and defaults to 1.0.";
        const std::string uvs_comment = "List of texture coordinates, in (u, [,v ,w]) coordinates, these will vary between 0 and 1. v, w are optional and default to 0.";
        const std::string normals_comment = "List of vertex normals in (x,y,z) form; normals might not be unit vectors.";
        const std::string polygons_comment = "Polygonal face element";

        // Add raw line to the list of lines
        inline void addLine(std::string line) { _lines.push_back(line + "\n"); }
        // Add formatted comment to the list of lines
        inline void addComment(std::string comment) { _lines.push_back("# " + comment + "\n"); }
        // Add empty line to the list of lines
        inline void addEmptyLine() { _lines.push_back("\n"); }

    public:
        ObjFileWriter(std::string filename) {
            _filename = filename;
        }

        // Add formatted vertices to the list of lines
        void addVertices(std::vector<Vector3d> vertices) {
            addComment(vertices_comment);
            for (const auto& v : vertices)
                addLine("v " + std::to_string(v.x) + " " + std::to_string(v.y) + " " + std::to_string(v.z));
            addEmptyLine();
        }

        // Add formatted uvs to the list of lines
        void addUVs(std::vector<Vector2d> uvs) {
            addComment(uvs_comment);
            for (const auto& uv : uvs)
                addLine("vt " + std::to_string(uv.x) + " " + std::to_string(uv.y));
            addEmptyLine();
        }

        // Add formatted normals to the list of lines
        void addNormals(std::vector<Vector3d> normals) {
            addComment(normals_comment);
            for (const auto& n : normals)
                addLine("vn " + std::to_string(n.x) + " " + std::to_string(n.y) + " " + std::to_string(n.z));
            addEmptyLine();
        }

        // Add formatted polygons to the list of lines
        void addPolygons(std::vector<int> vertices, std::vector<int> normals) {
            addComment(polygons_comment);
            // format: vertex_index/texture_index/normal_index
            // Starts with 1
            for (int i = 0; i < vertices.size(); i += 3) {
                addLine(
                    "f " +
                    std::to_string(vertices[i + 0]) + "/" + std::to_string(vertices[i + 0]) + "/" + std::to_string(normals[i + 0]) + " " +
                    std::to_string(vertices[i + 1]) + "/" + std::to_string(vertices[i + 1]) + "/" + std::to_string(normals[i + 1]) + " " +
                    std::to_string(vertices[i + 2]) + "/" + std::to_string(vertices[i + 2]) + "/" + std::to_string(normals[i + 2])
                );
            }
        }

        // Write the accumulated lines into the file
        void write() {
            std::ofstream file;
            file.open("../dist/" + _filename + ".obj");
            for (const auto& line : _lines)
                file << line;
            file.close();

            std::cout << "Written to " << _filename << ".obj" << std::endl;
        }
};

#endif
