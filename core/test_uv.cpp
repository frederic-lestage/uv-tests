#include "./test_uv.h"
#include "./file_writer.h"

#include <algorithm>
#include <vector>

void TestUV::uv_remap(std::vector<Vector2d>* uv) {
    // 1. Find the min and max values on both dimensions
    double minu, maxu;
    double minv, maxv;

    minu = minv = std::numeric_limits<double>::max();
    maxu = maxv = std::numeric_limits<double>::min();

    for (int i = 0; i < uv->size(); i++) {
        minu = uv->operator[](i).x < minu ? uv->operator[](i).x : minu;
        minv = uv->operator[](i).y < minv ? uv->operator[](i).y : minv;
        maxu = uv->operator[](i).x > maxu ? uv->operator[](i).x : maxu;
        maxv = uv->operator[](i).y > maxv ? uv->operator[](i).y : maxv;
    }

    // 2. Move points to origin, makes next step simpler and set pivot to
    // bottom-left corner
    for (int i = 0; i < uv->size(); i++) {
        uv->operator[](i).x -= minu;
        uv->operator[](i).y -= minv;
    }
    maxu = maxu - minu;
    maxv = maxv - minv;
    minu = minv = 0;

    // 2. Keep object ratio
    double a = maxu;
    double b = maxv;

    double min, max;

    if (a > b) {
        for (int i = 0; i < uv->size(); i++)
            uv->operator[](i) = Vector2d(uv->operator[](i).x, (b/a) * uv->operator[](i).y);
        min = minu;
        max = maxu;
    } else {
        for (int i = 0; i < uv->size(); i++)
            uv->operator[](i) = Vector2d((a/b) * uv->operator[](i).x, uv->operator[](i).y);
        min = minv;
        max = maxv;
    }

    // 3. Remap each vector from inside the box (min, max) to a 1x1 square so it can be used in a uv map
    for (int i = 0; i < uv->size(); i++) {
        uv->operator[](i).x = TestUV::remap(uv->operator[](i).x, min, max, 0, 1);
        uv->operator[](i).y = TestUV::remap(uv->operator[](i).y, min, max, 0, 1);
    }
}

//? old version and new version don't change result very much. It still requires
//? a change in the coordinate system.
//? But changes from (-p.y, p.z) to (-p.x, -p.z);
Matrix3d TestUV::get_rotation_matrix() {
    //https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d

    Vector3d a = eye_direction; //! unit vector
    Vector3d b = k;             //! unit vector

    // Crossproduct of 2 vectors starting at origin:
    // https://www.mathsisfun.com/algebra/vectors-cross-product.html
    Vector3d v = Vector3d(
        a.y * b.z - a.z + b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x
    );

    double angle = std::acos(Vector3d::dotProduct(a, b));
    // double s = v.magnitude() * std::sin(angle);
    double c = Vector3d::dotProduct(a, b) * std::cos(angle);

    Matrix3d I = Matrix3d(1, 0, 0, 0, 1, 0, 0, 0, 1);
    Matrix3d vx = Matrix3d(0, -v.z, v.y, v.z, 0, -v.x, -v.y, v.x, 0);
    Matrix3d vvx = vx * vx; //? Might not be the correct way
    vvx = vvx * (1 / (1 + c));

    Matrix3d R = I + vx + vvx;

    return R;

    // Old (naïve) version which wasn't doing what we wanted
    /*double alpha = acos(Vector3d::dotProduct(i, eye_direction));
    double beta = acos(Vector3d::dotProduct(j, eye_direction));
    double gamma = acos(Vector3d::dotProduct(k, eye_direction));

    Matrix3d Rx = Matrix3d(alpha, Matrix3d::X);
    Matrix3d Ry = Matrix3d(beta, Matrix3d::Y);
    Matrix3d Rz = Matrix3d(gamma, Matrix3d::Z);

    return Rx * Ry * Rz;*/
}

void TestUV::createObj() {
    eye_direction.normalize(); //! make sure eye_direction is a unit vector for testing

    ObjFileWriter writer(_filename);
    writer.addVertices(TestUV::vertices);
    writer.addNormals(TestUV::normals);

    writer.addUVs(generate_uvs(vertices, eye_position, eye_direction));

    writer.addPolygons(TestUV::polygons_vertices, TestUV::polygons_normals);
    writer.write();
}
