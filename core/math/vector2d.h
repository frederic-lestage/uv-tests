#ifndef VECTOR_2D_H
#define VECTOR_2D_H

/**
 * Simple implementation of a 2d vector to represent uv coordinates
 */
class Vector2d {
    public:
        double x, y;
        Vector2d(double x = 0, double y = 0) {
            this->x = x;
            this->y = y;
        }
};

#endif
