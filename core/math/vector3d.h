#ifndef VECTOR_3D_H
#define VECTOR_3D_H

#include <math.h>

/**
 * Simple implementation of a 3d vector
 */
class Vector3d {
    public:
        double x, y, z;
        Vector3d(double x = 0, double y = 0, double z = 0) {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        Vector3d(const Vector3d &other) {
            this->x = other.x;
            this->y = other.y;
            this->z = other.z;
        }

        double magnitude() {
            return std::sqrt(x*x + y*y + z*z);
        }

        double squared_magnitude() {
            return x*x + y*y + z*z;
        }

        void normalize() {
            double magnitude = this->magnitude();
            x = x/magnitude;
            y = y/magnitude;
            z = z/magnitude;
        }

        Vector3d normalized() {
            double magnitude = this->magnitude();
            return Vector3d(x / magnitude, y / magnitude, z / magnitude);
        }

        static double dotProduct(Vector3d& a, Vector3d& b) {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        Vector3d inverse() {
            return Vector3d(-x, -y, -z);
        }

        Vector3d operator+(const Vector3d& other) {
            return Vector3d(x + other.x, y + other.y, z + other.z);
        }

        Vector3d operator-(const Vector3d& other) {
            return Vector3d(x - other.x, y - other.y, z - other.z);
        }

        Vector3d operator*(const double& d) {
            return Vector3d(d * x, d * y, d * z);
        }

        Vector3d operator/(const double& d) {
            return Vector3d(x / d, y / d, z / d);
        }
};

#endif
