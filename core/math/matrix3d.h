#ifndef MATRIX_3D_H
#define MATRIX_3D_H

#include "./vector3d.h"

#include <vector>
#include <math.h>

/**
 * Simple implementation of a 3x3 matrix
 */
class Matrix3d {
    private:
        double storage[9] = {};

        void set_rotation_x(double angle) {
            storage[0] = 1;
            storage[4] = cos(angle);
            storage[5] = -sin(angle);
            storage[7] = sin(angle);
            storage[8] = cos(angle);
        };

        void set_rotation_y(double angle) {
            storage[0] = cos(angle);
            storage[2] = sin(angle);
            storage[4] = 1;
            storage[6] = -sin(angle);
            storage[8] = cos(angle);
        };

        void set_rotation_z(double angle) {
            storage[0] = cos(angle);
            storage[1] = -sin(angle);
            storage[3] = sin(angle);
            storage[4] = cos(angle);
            storage[8] = 1;
        };

    public:
        enum Rotation {X, Y, Z};

        Matrix3d() : storage{0,0,0,0,0,0,0,0,0} { }
        Matrix3d(
            double a, double b, double c,
            double d, double e, double f,
            double g, double h, double i
        ) : storage{a,b,c,d,e,f,g,h,i} { };

        Matrix3d(double angle, Rotation axis) : storage{0,0,0,0,0,0,0,0,0} {
            switch(axis) {
                case X: set_rotation_x(angle); break;
                case Y: set_rotation_y(angle); break;
                case Z: set_rotation_z(angle); break;
            }
        }

        Matrix3d operator*(const double& d) {
            return Matrix3d(
                d * storage[0], d * storage[1], d * storage[2],
                d * storage[3], d * storage[4], d * storage[5],
                d * storage[6], d * storage[7], d * storage[8]
            );
        }

        Vector3d operator*(const Vector3d& v) {
            return Vector3d(
                v.x * storage[0] + v.y * storage[1] + v.z * storage[2],
                v.x * storage[3] + v.y * storage[4] + v.z * storage[5],
                v.x * storage[6] + v.y * storage[7] + v.z * storage[8]
            );
        }

        Matrix3d operator*(Matrix3d& m) {
            Matrix3d matrix = Matrix3d();

            for(int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    for (int k = 0; k < 3; k++) {
                        matrix.increment(i, j, storage[i * 3 + k] * m(k, j));
                    }
                }
            }

            return matrix;
        }

        Matrix3d operator+(Matrix3d& m) {
            return Matrix3d(
                m(0, 0) + storage[0], m(0, 1) + storage[1], m(0, 2) + storage[2],
                m(1, 0) + storage[3], m(1, 1) + storage[4], m(1, 2) + storage[5],
                m(2, 0) + storage[6], m(2, 1) + storage[7], m(2, 2) + storage[8]
            );
        }

        double operator()(int i, int j) {
            return storage[i * 3 + j];
        }

        void increment(int i, int j, double v) {
            storage[i * 3 + j] += v;
        }

        /*void print() {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    std::cout << storage[i * 3 + j] << " ";
                }
                std::cout << std::endl;
            }
            std::cout << std::endl;
        }*/
};

#endif
