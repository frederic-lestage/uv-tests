#ifndef FLAT_PROJECTION_H
#define FLAT_PROJECTION_H

#include "../test_uv.h"

/**
 * Orthogonal (orthographic) projection from an object on a plane
 */
class FlatProjection : public TestUV {
    public:
        FlatProjection(
            std::vector<Vector3d> vertices, std::vector<Vector3d> normals,
            std::vector<int> polygons_vertices,
            std::vector<int> polygons_normals,
            std::string filename
        ) : TestUV(vertices, normals, polygons_vertices, polygons_normals, filename) { }

    protected:
        std::vector<Vector2d> generate_uvs(std::vector<Vector3d> vertices,
            Vector3d eye_position, Vector3d eye_direction
        ) override {
            std::vector<Vector2d> uvs;

            Vector3d plane_point, vertex_to_plane, p;
            Vector3d eye_dir_inverse = eye_direction.inverse();
            Vector2d uv;
            double l, angle, angle_opposed, vertex_plane_normal;

            Matrix3d rotation_matrix = get_rotation_matrix();

            for (auto& vertex : vertices) {
                plane_point = eye_position + eye_direction;
                vertex_to_plane = plane_point - vertex;
                l = vertex_to_plane.magnitude();

                angle = std::acos(Vector3d::dotProduct(vertex_to_plane, eye_dir_inverse) / l); //! rad
                angle_opposed = M_PI_2 - angle;

                vertex_plane_normal = l * sin(angle_opposed);
                p = vertex + (eye_dir_inverse * vertex_plane_normal);

                p = p - eye_direction; //TODO: might not be necessary here

                p = rotation_matrix * p;

                uvs.push_back(Vector2d(-p.x, -p.z));
            }
            /**
             * Scale the uv coordinates inside a 1x1 square
             */
            uv_remap(&uvs);

            return uvs;
        };
};

#endif
