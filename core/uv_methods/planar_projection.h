#ifndef PLANAR_PROJECTION_H
#define PLANAR_PROJECTION_H

#include "../test_uv.h"

/**
 * Planar projection from an object on a plane, takes into account perspective
 */
class PlanarProjection : public TestUV {
    public:
        PlanarProjection(
            std::vector<Vector3d> vertices, std::vector<Vector3d> normals,
            std::vector<int> polygons_vertices,
            std::vector<int> polygons_normals,
            std::string filename
        ) : TestUV(vertices, normals, polygons_vertices, polygons_normals, filename) { }

    protected:
        std::vector<Vector2d> generate_uvs(std::vector<Vector3d> vertices,
            Vector3d eye_position, Vector3d eye_direction
        ) override {
            std::vector<Vector2d> uvs;

            Vector3d delta, p;
            Vector2d uv;

            Matrix3d rotation_matrix = get_rotation_matrix();

            for (auto& vertex : vertices) {
                /**
                 * Project the vertex on a plane whose normal is -eye_direction
                 */

                // Move the point relative to origin instead of relative
                // to eye_position so it can accurately be compared with
                // eye_direction
                delta = vertex - eye_position;

                // Normalize the point
                delta.normalize();

                // Find the point along the vector delta which lies on the plane
                p = delta / Vector3d::dotProduct(delta, eye_direction);


                /**
                 * Transform the plane (translation + rotation) so it lies on
                 * the xy plane providing coordinates we can then translate to a
                 * 2d vector and use it for uv coordinates
                 */

                // Translate the plane (thus the point, relatively) to origin
                p = p - eye_direction;

                // Rotate the plane (thus the point, relatively) using the
                // rotation we've found at the beginning
                p = rotation_matrix * p;

                //TODO: figure out...
                // It's not exactly the xy plane, or it has to do with the
                // coordinate system which differs between blender (z-up) vs
                // .obj files (y-up?)
                uvs.push_back(Vector2d(-p.x, -p.z));
            }
            /**
             * Scale the uv coordinates inside a 1x1 square
             */
            uv_remap(&uvs);

            return uvs;
        };
};

#endif
