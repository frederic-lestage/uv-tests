#ifndef SPHERICAL_PROJECTION_H
#define SPHERICAL_PROJECTION_H

#include "../test_uv.h"

/**
 * Spherical projection from a spheric object on a plane
 * https://en.wikipedia.org/wiki/UV_mapping
 */
class SphericalProjection : public TestUV {
    public:
        SphericalProjection(
            std::vector<Vector3d> vertices, std::vector<Vector3d> normals,
            std::vector<int> polygons_vertices,
            std::vector<int> polygons_normals,
            std::string filename
        ) : TestUV(vertices, normals, polygons_vertices, polygons_normals, filename) { }

    protected:
        std::vector<Vector2d> generate_uvs(std::vector<Vector3d> vertices,
            Vector3d eye_position, Vector3d eye_direction
        ) override {
            std::vector<Vector2d> uvs;

            Vector3d point;
            Vector2d uv;
            double u, v;

            Matrix3d rotation_matrix = get_rotation_matrix();

            for (auto& vertex : vertices) {
                point = vertex;

                // Move "sphere" to origin : from eye_position
                point = point - eye_position;
                // Rotate each points : from eye_direction
                // point = rotation_matrix * point; //? does this matter: it does break thing with the axes

                // Calculate d, that being the unit vector from P to the sphere's origin (from Wikipedia)
                point = point.inverse();
                point.normalize();

                // Algorithm for UV on a sphere (from Wikipedia, but modified so it works, as seen in Calimiro)
                u = 0.5 + std::atan2(point.z, point.x) / M_PI; //? PI for ½sphere & 2*PI for full sphere
                v = 0.5 - std::asin(point.y) / M_PI;

                uvs.push_back(Vector2d(u, v));
            }
            /**
             * Scale the uv coordinates inside a 1x1 square
             */
            uv_remap(&uvs);

            return uvs;
        }
};

#endif
