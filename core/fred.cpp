#include "./fred.h"

#include <iostream>
#include <string>

int main(int ac, char** av) {
    if (ac < 4) {
        std::cout << "Usage: " << av[0] << " INPUT OUTPUT METHOD" << std::endl;
        std::cout << "    INPUT: source .obj file path (where f n//n, i.e. obj has vertex, normals, and no uvs)" << std::endl;
        std::cout << "    OUTPUT: name of the generated .obj" << std::endl;
        std::cout << "    METHOD:" << std::endl;
        std::cout << "        - 0 = planar projection (orthographic)" << std::endl;
        std::cout << "        - 1 = planar projection (perspective)" << std::endl;
        std::cout << "        - 2 = dome (½ sphere)" << std::endl;
        return 1;
    }

    ObjFileReader reader(av[1]);
    reader.read();

    std::string output = av[2];

    TestUV *uv;
    switch(std::stoi(av[3]) - std::stoi("0")) {
        case 0: {
            uv = new FlatProjection(
                reader.get_vertices(), reader.get_normals(),
                reader.get_polygons_vertices(),
                reader.get_polygons_normals(),
                output
            );
            break;
        }
        case 1: {
            uv = new PlanarProjection(
                reader.get_vertices(), reader.get_normals(),
                reader.get_polygons_vertices(),
                reader.get_polygons_normals(),
                output
            );
            break;
        }
        case 2: {
            uv = new SphericalProjection(
                reader.get_vertices(), reader.get_normals(),
                reader.get_polygons_vertices(),
                reader.get_polygons_normals(),
                output
            );
            break;
        }
    }
    uv->createObj();

    delete uv;
    return 0;
}
