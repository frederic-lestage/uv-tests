cmake_minimum_required(VERSION 3.10)
project(fred)
file(GLOB SOURCES "./*.cpp")
add_executable(fred ${SOURCES})
