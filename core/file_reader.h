#ifndef FILE_READER_H
#define FILE_READER_H

#include "./math/vector3d.h"
#include "./math/vector2d.h"

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

class ObjFileReader {
    private:
        std::string _path;

        std::vector<Vector3d> vertices;
        std::vector<Vector3d> normals;
        std::vector<int> polygons_vertices;
        std::vector<int> polygons_normals;

    public:
        ObjFileReader(std::string path) {
            _path = path;
        }

        void read() {
            // https://stackoverflow.com/questions/21120699/c-obj-file-parser
            std::ifstream in(_path, std::ios::in);
            if (!in) {
                std::cout << "Cannot open " << _path << std::endl;
                exit(1);
            }

            std::string line;
            while (std::getline(in, line))
            {
                if (line.substr(0,2) == "v "){
                    // check v for vertices
                    std::istringstream v(line.substr(2));
                    double x, y, z;
                    v >> x; v >> y; v >> z;
                    vertices.push_back(Vector3d(x, y, z));
                } else if (line.substr(0, 2) == "vn") {
                    // check vn for normals
                    std::istringstream n(line.substr(2));
                    double x, y, z;
                    n >> x; n >> y; n >> z;
                    normals.push_back(Vector3d(x, y, z));
                } else if(line.substr(0,2)=="f "){
                    //check for faces
                    int a, b, c; //to store mesh index
                    int A, B, C; //to store normal index

                    const char* chh = line.c_str();
                    sscanf (chh, "f %i//%i %i//%i %i//%i",&a,&A,&b,&B,&c,&C); //here it read the line start with f and store the corresponding values in the variables

                    polygons_vertices.push_back(a); polygons_normals.push_back(A);
                    polygons_vertices.push_back(b); polygons_normals.push_back(B);
                    polygons_vertices.push_back(c); polygons_normals.push_back(C);
                }

            };
        }

        std::vector<Vector3d> get_vertices() { return vertices; }
        std::vector<Vector3d> get_normals() { return normals; }
        std::vector<int> get_polygons_vertices() { return polygons_vertices; }
        std::vector<int> get_polygons_normals() { return polygons_normals; }
};

#endif
