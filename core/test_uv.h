#ifndef TEST_UV_H
#define TEST_UV_H

#include "./file_reader.h"
#include "./math/vector3d.h"
#include "./math/vector2d.h"
#include "./math/matrix3d.h"

#include <vector>
#include <string>

/**
 * Generic helper parent class for uv generation methods
 */

//! Blender (x, y, z)
//! .obj local space (x, y, z)
//! .obj global space (x, -z, y)
class TestUV {
    private:
        // Utils vectors for plane transform
        Vector3d i = Vector3d(1, 0, 0);
        Vector3d j = Vector3d(0, 1, 0);
        Vector3d k = Vector3d(0, 0, 1);

        Vector3d eye_position = Vector3d(0, 0, 0);
        Vector3d eye_direction = Vector3d(0, 0, 1); //! unit vector

        std::vector<Vector3d> normals;
        std::vector<Vector3d> vertices;
        std::vector<int> polygons_vertices;
        std::vector<int> polygons_normals;

        std::string _filename;

        /**
         * Transforms a value between 2 numbers onto a corresponding value
         * (same ratio) between 2 other numbers
         */
        inline double remap(
            double value,
            double from1, double to1,
            double from2, double to2
        ) {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

    protected:
        /**
         * Scale a map of points of arbitrary size into a square 1x1 map.
         * Changes the actual values of the vector
         */
        void uv_remap(std::vector<Vector2d>* uv);

        /**
         * Find the rotation to apply to the plane defined by -eye_direction
         * as its normal, so it lies in the xy plane
         */
        Matrix3d get_rotation_matrix();

        /**
         * Generate the uvs for the .obj file
         */
        virtual std::vector<Vector2d> generate_uvs(
            std::vector<Vector3d> vertices,
            Vector3d eye_position, Vector3d eye_direction
        ) =0;

    public:
        TestUV(
            std::vector<Vector3d> vertices, std::vector<Vector3d> normals,
            std::vector<int> polygons_vertices,
            std::vector<int> polygons_normals,
            std::string filename
        ) {
            this->vertices = vertices;
            this->normals = normals;
            this->polygons_vertices = polygons_vertices;
            this->polygons_normals = polygons_normals;
            _filename = filename;
        }

        /**
         * Create an .obj file
         */
        void createObj();
};

#endif
