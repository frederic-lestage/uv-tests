#ifndef FRED_H
#define FRED_H

#include "./file_reader.h"
#include "./test_uv.h"
#include "./uv_methods/flat_projection.h"
#include "./uv_methods/planar_projection.h"
#include "./uv_methods/spherical_projection.h"

#endif
